package com.androidcourse.notepadkotlin.ui.edit

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.androidcourse.notepadkotlin.R
import com.androidcourse.notepadkotlin.model.Game
import kotlinx.android.synthetic.main.activity_edit.*
import kotlinx.android.synthetic.main.content_edit.*
import java.util.*

class AddGameActivity : AppCompatActivity() {

    private lateinit var addGameViewModel: AddGameViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Add game"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initViews()
        initViewModel()
    }

    private fun initViews() {
        fab.setOnClickListener {
            var date = Calendar.getInstance()
            var day =etDay.text.toString().toInt()
            var month =etMonth.text.toString().toInt()-1
            var year = etYear.text.toString().toInt()
            date.set(year,month,day)
            var game=Game(etTitle.text.toString(),date.time,etPlatform.text.toString())
            addGameViewModel.insertGame(game)
        }
    }

    private fun initViewModel() {
        addGameViewModel = ViewModelProvider(this).get(AddGameViewModel::class.java)

        addGameViewModel.error.observe(this, Observer { message ->
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        })
        addGameViewModel.success.observe(this, Observer { success ->
            if (success) finish()
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> { // Used to identify when the user has clicked the back button
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_NOTE = "EXTRA_NOTE"
    }

}
