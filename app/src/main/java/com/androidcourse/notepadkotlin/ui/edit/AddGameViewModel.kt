package com.androidcourse.notepadkotlin.ui.edit

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.androidcourse.notepadkotlin.database.GameRepository
import com.androidcourse.notepadkotlin.model.Game
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AddGameViewModel(application: Application) : AndroidViewModel(application) {

    private val gameRepository = GameRepository(application.applicationContext)
    private val mainScope = CoroutineScope(Dispatchers.Main)


    val error = MutableLiveData<String?>()
    val success = MutableLiveData<Boolean>()

    fun insertGame(game:Game){
        if (isGameValid(game)) {
            mainScope.launch {
                withContext(Dispatchers.IO) {
                    gameRepository.insertGame(game)
                }
            }
            success.value=true;
        }
    }


    private fun isGameValid(game:Game): Boolean {
        return when {
            game == null -> {
                error.value = "game may not be null."
                false
            }
            game.title=="" -> {
                error.value = "Title must not be empty"
                false
            }
            else -> true
        }
    }
}