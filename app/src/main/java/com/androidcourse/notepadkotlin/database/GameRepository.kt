package com.androidcourse.notepadkotlin.database

import android.content.Context
import androidx.lifecycle.LiveData
import com.androidcourse.notepadkotlin.model.Game

class GameRepository(context: Context) {

    private val gameDao: GameDao

    init {
        val database = GameRoomDatabase.getDatabase(context)
        gameDao = database!!.gameDao()
    }

    fun getAllGames() : LiveData<List<Game>> {
        return gameDao.getGames()
    }


    suspend fun deleteGame(game: Game) {
        gameDao?.deleteGame(game)
    }
    suspend fun deleteAllGames() {
        gameDao?.deleteAllGames()
    }


    suspend fun insertGame(game: Game) {
        gameDao.insertGame(game)
    }


}