package com.androidcourse.notepadkotlin.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.androidcourse.notepadkotlin.model.Game

@Dao
interface GameDao {

    @Insert
    suspend fun insertGame(game: Game)

    @Query("SELECT * FROM Game ORDER BY releaseDate ASC")
    fun getGames() : LiveData<List<Game>>

    @Update
    suspend fun updateGame(game: Game)

    @Delete
    suspend fun deleteGame(game: Game)

    @Query("DELETE FROM Game")
    suspend fun deleteAllGames()

}