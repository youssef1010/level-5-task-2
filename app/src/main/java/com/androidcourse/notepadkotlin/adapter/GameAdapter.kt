package com.androidcourse.notepadkotlin.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.androidcourse.notepadkotlin.R
import com.androidcourse.notepadkotlin.model.Game
import kotlinx.android.synthetic.main.item_game.view.*
import java.text.SimpleDateFormat
import java.util.*

public class GameAdapter(private val matches: List<Game>) :
    RecyclerView.Adapter<GameAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_game, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return matches.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(matches[position])
    }


    fun dateFormat(receivedDate: Date): String {
        var pattern ="MMM-d-yyyy"
        var format: SimpleDateFormat? = SimpleDateFormat(pattern)
        var formatDate = format?.format(receivedDate).toString()
        return formatDate
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(game : Game) {

            itemView.tvTitle.text = game.title;
            itemView.tvPlatform.text = game.title;
            itemView.tvDate.text = dateFormat(game.releaseDate);


        }
    }
}